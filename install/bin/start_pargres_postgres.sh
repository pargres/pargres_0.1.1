#!/bin/bash

# Shell script para inicialização do ParGRES com Postgres.
# Parâmetros:
#  - $1 - diretorio de trabalho na area de SCRATCH
#  - $2 - arquivo com os nomes dos nós alocados para o job
#  - $3 - diretorio de instalacao do PostgreSQL
#  - $4 - diretorio onde se encontra a base de dados. Deve ter o mesmo nome em todos os nos do cluster.
#  - $4 - nome do banco de dados
#  - $5 - nome de usuario no banco de dados
#  - $6 - senha do usuario no banco de dados

if [ $# -ne 7 ];
then
    echo "usage: start_pargres_postgres.sh WORKDIR NODEFILE POSTGRES_HOME DATADIR DBNAME DBLOGIN DBPASSWORD"
    exit 2
fi

WORKDIR=$1
NODEFILE=$2
PG_HOME=$3
DATADIR=$4
DBNAME=$5
DBLOGIN=$6
DBPASSWORD=$7 # Com string vazia (""), dá problema.

DIR_INICIAL=`pwd`

if [ -z "$PARGRES_HOME" ]; then
    PARGRES_HOME=$(dirname "$(readlink -f "$0")")
    PARGRES_HOME=${PARGRES_HOME%/*}
fi

# Criacao do diretorio de trabalho do job, caso não exista
echo "Criando diretorio" $WORKDIR
mkdir -p $WORKDIR

# Diretorio para arquivos de configuracao utilizados no job
CONFIG_DIR=$WORKDIR/config
mkdir -p $CONFIG_DIR

# Copia dos arquivos de configuracao para diretorio de trabalho
cp $PARGRES_HOME/config/* $CONFIG_DIR/

# Criacao do arquivo config.xml
java -cp $PARGRES_HOME/bin WriteConfig $NODEFILE user '' hostname
mv config.xml $CONFIG_DIR

i=0
for LINE in $(cat $NODEFILE)
do
    echo $DATADIR
    ssh $LINE "cd $PG_HOME/bin; \
    $PG_HOME/bin/pg_ctl -D $DATADIR start; \
    $PG_HOME/bin/pg_ctl -D $DATADIR status; \
    sleep 20; \
    cd $PARGRES_HOME/bin; \
    ./run_nqp.sh 3001 $DBNAME $DBLOGIN $DBPASSWORD 0 > $WORKDIR/run_nqp_$i.out 2> $WORKDIR/run_nqp_$i.err" &
    sleep 10
    let i=$i+1
done

sleep 20
cd $PARGRES_HOME/bin
chmod +x ./run_cqp.sh
./run_cqp.sh $CONFIG_DIR/config.xml  > $WORKDIR/run_cqp.out 2> $WORKDIR/run_cqp.err &
sleep 20

cd $DIR_INICIAL
exit 0
