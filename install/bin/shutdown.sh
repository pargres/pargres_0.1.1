#!/bin/bash
HOME_PARGRES=../
LIB=$HOME_PARGRES/lib
java -Xdebug -Xrunjdwp:transport=dt_socket,address=8009,server=y,suspend=n -Xmx512m -Dlog4j.debug -cp $HOME:$LIB/hsqldb.jar:$LIB/log4j-1.2.9.jar:$LIB/pargres-server.jar:$LIB/postgresql-42.2.8.jar org.pargres.cqp.connection.ConnectionManagerAdmin 8050
