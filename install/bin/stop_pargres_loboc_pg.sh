#!/bin/bash

# Shell script para desativacao do ParGRES com Postgres no Lobo Carneiro, que tem disco compartilhado
# Parâmetros:
#  - $1 - diretorio de trabalho na area de SCRATCH
#  - $2 - arquivo com os nomes dos nós alocados para o job
#  - $3 - diretorio de instalacao do PostgreSQL

if [ $# -ne 3 ];
then
    echo "usage: stop_pargres_loboc_pg.sh WORKDIR NODEFILE POSTGRES_HOME"
    exit 2
fi

WORKDIR=$1
NODEFILE=$2
PG_HOME=$3

DIR_INICIAL=`pwd`

# PARGRES_HOME vai ficar aqui para quando for possivel fazer shutdown pelo ParGRES
if [ -z "$PARGRES_HOME" ]; then
    PARGRES_HOME=$(dirname "$(readlink -f "$0")")
    PARGRES_HOME=${PARGRES_HOME%/*}
fi

cd $PARGRES_HOME/bin
chmod +x shutdown.sh
./shutdown.sh

i=0
for LINE in $(cat $NODEFILE)
do
    DATADIR=$WORKDIR/datadir_$i
    echo $DATADIR
    ssh $LINE "cd $PG_HOME/bin; \
    $PG_HOME/bin/pg_ctl -D $DATADIR stop; \
    $PG_HOME/bin/pg_ctl -D $DATADIR status" &
    sleep 10
    let i=$i+1
done

cd $DIR_INICIAL
exit 0
