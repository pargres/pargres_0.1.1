#!/bin/bash

# Shell script para inicialização do ParGRES com Postgres no Lobo Carneiro, que tem disco compartilhado
# Parâmetros:
#  - $1 - diretorio de trabalho na area de SCRATCH
#  - $2 - arquivo com os nomes dos nós alocados para o job
#  - $3 - diretorio de instalacao do MySQL
#  - $4 - nome do banco de dados
#  - $5 - nome de usuario no banco de dados
#  - $6 - senha do usuario no banco de dados

if [ $# -ne 6 ];
then
    echo "usage: start_pargres_loboc_mysql.sh WORKDIR NODEFILE MYSQL_HOME DBNAME DBLOGIN DBPASSWORD"
    exit 2
fi

WORKDIR=$1
NODEFILE=$2
MYSQL_HOME=$3
DBNAME=$4
DBLOGIN=$5
DBPASSWORD=$6 # Com string vazia (""), dá problema.

DIR_INICIAL=`pwd`

if [ -z "$PARGRES_HOME" ]; then
    PARGRES_HOME=$(dirname "$(readlink -f "$0")")
    PARGRES_HOME=${PARGRES_HOME%/*}
fi

i=0
for LINE in $(cat $NODEFILE)
do
    JOBDIR=$WORKDIR/mysql_$i
    MYSQL_TMPDIR=$JOBDIR/tmp
    echo "Nó" $LINE"..."
    ssh $LINE "cd $MYSQL_HOME/bin; \
    mkdir -p $JOBDIR; \
    mkdir -p $MYSQL_TMPDIR; \
    echo \"Invocando mysqld...\"; \
    nohup mysqld --defaults-file=$MYSQL_HOME/my.cnf --pid-file=$JOBDIR/`hostname`.pid --tmpdir=$MYSQL_TMPDIR --socket=$JOBDIR/mysql.`hostname`.sock --mysqlx-socket=$JOBDIR/mysqlx.`hostname`.sock --general_log_file=$JOBDIR/general.log --log-bin=$JOBDIR/logbin.log --slow_query_log_file=$JOBDIR/slowquery.log --log-error=$JOBDIR/`hostname`.err > $JOBDIR/mysqld_`hostname`.out 2> $JOBDIR/mysqld_`hostname`.out & \
    sleep 20; \
    module load java; \
    cd $PARGRES_HOME/bin; \
    chmod +x ./run_nqp_mysql.sh; \
    echo \"Invocando run_nqp_mysql.sh...\"; \
    nohup ./run_nqp_mysql.sh 3001 $DBNAME $DBLOGIN $DBPASSWORD 0 > $WORKDIR/run_nqp_mysql_$i.out 2> $WORKDIR/err_run_nqp_mysql_$i.err & \
    echo \"NQP invocado.\" " & 
    sleep 20
    let i=$i+1
done
echo "Servidores de BD e NQPs no ar"
sleep 1m
cd $PARGRES_HOME/bin
chmod +x ./run_cqp_mysql.sh
echo "Executando CPQ..."
./run_cqp_mysql.sh $WORKDIR/config/config.xml  > $WORKDIR/run_cqp_mysql.out 2> $WORKDIR/run_cqp_mysql.err &
echo "Início da pausa para carga de metadados:" `date`
sleep 1m
echo "Final da pausa para carga de metadados:" `date`

cd $DIR_INICIAL
echo "Fim start"
exit 0
