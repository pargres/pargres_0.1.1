#!/bin/bash

# Shell script para inicializar um NodeQueryProcessor do ParGRES
# Parametros:
#   $1 - porta NQP
#   $2 - nome do BD
#   $3 - usuario do BD
#   $4 - senha
#   $5 - flag indicando se intervalos de datas devem ter aspas (1) ou nao (0)

if [ $# -ne 5 ];
then
    echo "usage: run_nqp_mysql.sh NQPPORT DBNAME DBLOGIN DBPASSWORD QUOTEDDATEINTERVAL[0|1]"
    exit 2
fi

NQPPORT=$1
DBNAME=$2
DBLOGIN=$3
DBPASSWORD=$4
QUOTEDDATEINTERVAL=$5

DIR_INICIAL=`pwd`

if [ -z "$PARGRES_HOME" ]; then
    PARGRES_HOME=$(dirname "$(readlink -f "$0")")
    PARGRES_HOME=${PARGRES_HOME%/*}
fi

LIB=$PARGRES_HOME/lib

java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8002 -Xms2g -Xmx2g -cp $LIB/hsqldb_2019.jar:$LIB/log4j-1.2.9.jar:$LIB/pargres-server.jar:$LIB/mysql-connector-java-8.0.19.jar org.pargres.nodequeryprocessor.NodeQueryProcessorEngine $NQPPORT com.mysql.cj.jdbc.Driver jdbc:mysql://localhost:3306/$DBNAME $DBLOGIN $DBPASSWORD $QUOTEDDATEINTERVAL


exit 0
