#!/bin/bash

# Shell script para inicializar um NodeQueryProcessor do ParGRES usando SQLite como SGBD
# Parametros:
#   $1 - porta NQP
#   $2 - caminho completo do arquivo JAR que contém o DuckDB JDBC 
#   $3 - nome do BD
#   $4 - flag indicando se intervalos de datas devem ter aspas (1) ou nao (0)

if [ $# -ne 4 ];
then
    echo "usage: run_nqp_sqlite.sh NQPPORT SQLITE_JDBC_JAR DBNAME QUOTEDDATEINTERVAL[0|1]"
    exit 2
fi

NQPPORT=$1
DUCKDB_JDBC_JAR=$2
DBNAME=$3
QUOTEDDATEINTERVAL=$4

DIR_INICIAL=`pwd`

if [ -z "$PARGRES_HOME" ]; then
    PARGRES_HOME=$(dirname "$(readlink -f "$0")")
    PARGRES_HOME=${PARGRES_HOME%/*}
fi

LIB=$PARGRES_HOME/lib

java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8002 -Xms2g -Xmx2g -cp $LIB/hsqldb_2019.jar:$LIB/log4j-1.2.9.jar:$LIB/pargres-server.jar:$DUCKDB_JDBC_JAR org.pargres.nodequeryprocessor.NodeQueryProcessorEngine $NQPPORT org.duckdb.DuckDBDriver jdbc:duckdb:$DBNAME "" "" $QUOTEDDATEINTERVAL


exit 0
