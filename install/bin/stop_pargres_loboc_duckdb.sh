#!/bin/bash

# Shell script para desativacao do ParGRES com Postgres no Lobo Carneiro, que tem disco compartilhado
# Parâmetros:
#  - $1 - diretorio de trabalho na area de SCRATCH
#  - $2 - arquivo com os nomes dos nós alocados para o job

if [ $# -ne 2 ];
then
    echo "usage: stop_pargres_loboc_pg.sh WORKDIR NODEFILE"
    exit 2
fi

WORKDIR=$1
NODEFILE=$2

DIR_INICIAL=`pwd`

# PARGRES_HOME vai ficar aqui para quando for possivel fazer shutdown pelo ParGRES
if [ -z "$PARGRES_HOME" ]; then
    PARGRES_HOME=$(dirname "$(readlink -f "$0")")
    PARGRES_HOME=${PARGRES_HOME%/*}
fi

i=0
for LINE in $(cat $NODEFILE)
do
    JOBDIR=$WORKDIR/mysql_$i; 
    ssh $LINE "cd $PARGRES_HOME/bin; \
    ./shutdown.sh; \
    sleep 20;" & 
    sleep 10
    let i=$i+1
done
sleep 30
cd $PARGRES_HOME/bin
./shutdown.sh

cd $DIR_INICIAL
exit 0
