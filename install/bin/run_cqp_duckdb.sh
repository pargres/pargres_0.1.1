#!/bin/bash

# Shell script para inicializar o ClusterQueryProcessor do ParGRES
# Parametros:
#   $1 - arquivo de configuracao em XML

if [ $# -ne 1 ];
then
    echo "usage: run_cqp_duckdb.sh CONFIG_FILE"
    exit 2
fi

CONFIG_FILE=$1

DIR_INICIAL=`pwd`

if [ -z "$PARGRES_HOME" ]; then
    PARGRES_HOME=$(dirname "$(readlink -f "$0")")
    PARGRES_HOME=${PARGRES_HOME%/*}
fi

LIB=$PARGRES_HOME/lib

java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8001 -Xmx512m -Dlog4j.debug -cp $HOME:$LIB/hsqldb_2019.jar:$LIB/log4j-1.2.9.jar:$LIB/pargres-server.jar org.pargres.cqp.connection.ConnectionManagerImpl 8050 $CONFIG_FILE

exit 0
