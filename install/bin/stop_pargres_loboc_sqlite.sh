#!/bin/bash

# Shell script para desativacao do ParGRES com Postgres no Lobo Carneiro, que tem disco compartilhado
# Parâmetros:
#  - $1 - diretorio de trabalho na area de SCRATCH
#  - $2 - arquivo com os nomes dos nós alocados para o job

if [ $# -ne 2 ];
then
    echo "usage: stop_pargres_loboc_pg.sh WORKDIR NODEFILE"
    exit 2
fi

WORKDIR=$1
NODEFILE=$2

DIR_INICIAL=`pwd`

# PARGRES_HOME vai ficar aqui para quando for possivel fazer shutdown pelo ParGRES
if [ -z "$PARGRES_HOME" ]; then
    PARGRES_HOME=$(dirname "$(readlink -f "$0")")
    PARGRES_HOME=${PARGRES_HOME%/*}
fi


cd $PARGRES_HOME/bin
chmod +x shutdown.sh
./shutdown.sh

cd $DIR_INICIAL
exit 0
