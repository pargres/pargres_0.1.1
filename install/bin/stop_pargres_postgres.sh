#!/bin/bash

# Shell script para desativacao do ParGRES com Postgres.
# Parâmetros:
#  - $1 - diretorio de trabalho na area de SCRATCH
#  - $2 - arquivo com os nomes dos nós alocados para o job
#  - $3 - diretorio de instalacao do PostgreSQL
#  - $4 - diretorio onde se encontra a base de dados. Deve ter o mesmo nome em todos os nos do cluster.

if [ $# -ne 4 ];
then
    echo "usage: stop_pargres_postgres.sh WORKDIR NODEFILE POSTGRES_HOME DATADIR"
    exit 2
fi

WORKDIR=$1
NODEFILE=$2
PG_HOME=$3
DATADIR=$4

DIR_INICIAL=`pwd`

# PARGRES_HOME vai ficar aqui para quando for possivel fazer shutdown pelo ParGRES
if [ -z "$PARGRES_HOME" ]; then
    PARGRES_HOME=$(dirname "$(readlink -f "$0")")
    PARGRES_HOME=${PARGRES_HOME%/*}
fi

i=0
for LINE in $(cat $NODEFILE)
do
    ssh $LINE "killall java; \
    cd $PG_HOME/bin; \
    $PG_HOME/bin/pg_ctl -D $DATADIR stop; \
    $PG_HOME/bin/pg_ctl -D $DATADIR status" &
    sleep 10
    let i=$i+1
done
sleep 20
cd $PARGRES_HOME/bin
killall java

cd $DIR_INICIAL
exit 0
