#!/bin/bash

# Shell script para inicialização do ParGRES com Postgres no Lobo Carneiro, que tem disco compartilhado
# Parâmetros:
#  - $1 - diretorio de trabalho na area de SCRATCH
#  - $2 - arquivo com os nomes dos nós alocados para o job
#  - $3 - diretorio de instalacao do PostgreSQL
#  - $4 - nome do banco de dados
#  - $5 - nome de usuario no banco de dados
#  - $6 - senha do usuario no banco de dados

if [ $# -ne 6 ];
then
    echo "usage: start_pargres_loboc_pg.sh WORKDIR NODEFILE POSTGRES_HOME DBNAME DBLOGIN DBPASSWORD"
    exit 2
fi

WORKDIR=$1
NODEFILE=$2
PG_HOME=$3
DBNAME=$4
DBLOGIN=$5
DBPASSWORD=$6 # Com string vazia (""), dá problema.

DIR_INICIAL=`pwd`

if [ -z "$PARGRES_HOME" ]; then
    PARGRES_HOME=$(dirname "$(readlink -f "$0")")
    PARGRES_HOME=${PARGRES_HOME%/*}
fi

i=0
for LINE in $(cat $NODEFILE)
do
    DATADIR=$WORKDIR/datadir_$i
    echo "DATADIR = "$DATADIR

    echo "ssh $LINE \"cd $PG_HOME/bin; \
    $PG_HOME/bin/pg_ctl -D $DATADIR -o \"-c unix_socket_directories=\" start; \
    $PG_HOME/bin/pg_ctl -D $DATADIR status; \
    sleep 20; \
    module load java; \
    cd $PARGRES_HOME/bin; \
    nohup ./run_nqp.sh 3001 $DBNAME $DBLOGIN $DBPASSWORD 0 > $WORKDIR/run_nqp_$i.out 2> $WORKDIR/err_run_nqp_$i.err &\""

    ssh $LINE "cd $PG_HOME/bin; \
    $PG_HOME/bin/pg_ctl -D $DATADIR -o \"-c unix_socket_directories=\" start; \
    $PG_HOME/bin/pg_ctl -D $DATADIR status; \
    sleep 20; \
    module load java; \
    cd $PARGRES_HOME/bin; \
    chmod +x ./run_nqp.sh; \
    nohup ./run_nqp.sh 3001 $DBNAME $DBLOGIN $DBPASSWORD 0 > $WORKDIR/run_nqp_$i.out 2> $WORKDIR/err_run_nqp_$i.err &" &
    sleep 20
    let i=$i+1
done

sleep 2m
cd $PARGRES_HOME/bin
chmod +x ./run_cqp.sh
./run_cqp.sh $WORKDIR/config/config.xml  > $WORKDIR/run_cqp_o.out 2> $WORKDIR/run_cqp_e.err &
sleep 2m

cd $DIR_INICIAL
exit 0
