#!/bin/bash

# Shell script para preparar os nós do Lobo Carneiro envolvidos na execução de um JOB ParGRES.
# O LoboC tem arquitetura shared disk, que não é a ideal para o ParGRES. Portanto, é preciso
# replicar a base de dados para simular um ambiente com arquitetura shared nothing. Além disso,
# vários jobs podem estar em execução ao mesmo tempo, cada um executando sua instância ParGRES.
# Por isso, um diretório de trabalho é criado para cada job.
#
# Parâmetros:
#  - $1 - diretorio de trabalho na area de SCRATCH
#  - $2 - diretorio onde se encontra o ParGRES
#  - $3 - diretorio onde se encontra o banco de dados a ser replicado
#  - $4 - arquivo com os nomes dos nós alocados para o job
#  - $5 - 0 ou 1: indica se o diretório de trabalho deve ser criado com stripe(1) de arquivos ou não(0)

if [ $# -ne 5 ];
then
    echo "usage: pargres_staging_loboc_pg.sh WORKDIR PARGRES_HOME DATA_DIR NODEFILE DIRETORIO_TRABALHO_SEM[0]_OU_COM_STRIPE[1]"
    exit 2
fi

WORKDIR=$1
PARGRES_HOME=$2
DATA_DIR=$3
NODEFILE=$4
FILESTRIPE=$5


# Criacao do diretorio de trabalho do job, caso não exista
mkdir -p $WORKDIR
if [ $FILESTRIPE -eq 1 ];
then
    echo "Usando file stripe."
    lfs setstripe -c -1 $WORKDIR
else
    echo "Sem utilizar file stripe"
fi

# Diretorio para arquivos de configuracao utilizados no job
CONFIG_DIR=$WORKDIR/config
mkdir -p $CONFIG_DIR

# Copia dos arquivos de configuracao para diretorio de trabalho
#cp $PARGRES_HOME/config/* $CONFIG_DIR/

# Criacao do arquivo config.xml
echo "NODEFILE"
cat $NODEFILE
echo "config.xml"
cd $CONFIG_DIR
#java -cp $PARGRES_HOME/bin WriteConfig $NODEFILE user '' hostname
cp $PARGRES_HOME/config/config.xml.begin ./config.xml
for NO in $(cat $NODEFILE)
do
    echo -e "\t\t<node host='$NO' port='3001'/>" >> config.xml
done
cat $PARGRES_HOME/config/config.xml.end >> config.xml
cat config.xml

# Replicar a base de dados
i=0
for line in $(cat $NODEFILE)
do
    mkdir -p $WORKDIR/datadir_$i
    cp -rfp $DATA_DIR/* $WORKDIR/datadir_$i
    let i=$i+1
done

exit 0
