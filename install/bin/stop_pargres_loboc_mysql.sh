#!/bin/bash

# Shell script para desativacao do ParGRES com Postgres no Lobo Carneiro, que tem disco compartilhado
# Parâmetros:
#  - $1 - diretorio de trabalho na area de SCRATCH
#  - $2 - arquivo com os nomes dos nós alocados para o job
#  - $3 - diretorio de instalacao do MySQL

if [ $# -ne 3 ];
then
    echo "usage: stop_pargres_loboc_mysql.sh WORKDIR NODEFILE MYSQL_HOME"
    exit 2
fi

WORKDIR=$1
NODEFILE=$2
MYSQL_HOME=$3

DIR_INICIAL=`pwd`

# PARGRES_HOME vai ficar aqui para quando for possivel fazer shutdown pelo ParGRES
if [ -z "$PARGRES_HOME" ]; then
    PARGRES_HOME=$(dirname "$(readlink -f "$0")")
    PARGRES_HOME=${PARGRES_HOME%/*}
fi

cd $PARGRES_HOME/bin
chmod +x shutdown.sh
./shutdown.sh

i=0
for LINE in $(cat $NODEFILE)
do
    JOBDIR=$WORKDIR/mysql_$i; 
    ssh $LINE "cd $MYSQL_HOME/bin; \
    mysqladmin -u root -psenha --socket=$JOBDIR/mysql.`hostname`.sock shutdown" &
    sleep 10
    let i=$i+1
done


cd $DIR_INICIAL
exit 0
