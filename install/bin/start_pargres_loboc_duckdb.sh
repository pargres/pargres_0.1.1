#!/bin/bash

# Shell script para inicialização do ParGRES com Postgres no Lobo Carneiro, que tem disco compartilhado, usando o SQLite 3 via JDBC.
# Parâmetros:
#  - $1 - diretorio de trabalho na area de SCRATCH
#  - $2 - arquivo com os nomes dos nós alocados para o job
#  - $3 - caminho completo do arquivo JAR que contém o SQLite JDBC 
#  - $4 - nome do banco de dados

if [ $# -ne 4 ];
then
    echo "usage: start_pargres_loboc_duckdb.sh WORKDIR NODEFILE SQLITE_JDBC_JAR DBNAME"
    exit 2
fi

WORKDIR=$1
NODEFILE=$2
DUCKDB_JDBC_JAR=$3
DBNAME=$4

DIR_INICIAL=`pwd`

if [ -z "$PARGRES_HOME" ]; then
    PARGRES_HOME=$(dirname "$(readlink -f "$0")")
    PARGRES_HOME=${PARGRES_HOME%/*}
fi

BD_TMPDIR_BASE=$WORKDIR/tmp
i=0
for LINE in $(cat $NODEFILE)
do
    BD_TMPDIR=$BD_TMPDIR_BASE/$i
    echo "Nó" $LINE"..."
    ssh $LINE "module load java; \
    mkdir -p $BD_TMPDIR; \
    export DUCKDB_TMPDIR=$BD_TMPDIR; \
    cd $PARGRES_HOME/bin; \
    chmod +x ./run_nqp_duckdb.sh; \
    echo \"Invocando run_nqp_duck.sh...\"; \
    LD_PRELOAD="/scratch/21061a/pedrocavaliere/lib223/lib/libm.so.6" nohup ./run_nqp_duckdb.sh 3001 $DUCKDB_JDBC_JAR $DBNAME 0 > $WORKDIR/run_nqp_duckdb_$i.out 2> $WORKDIR/err_run_nqp_duckdb_$i.err & \
    echo \"NQP invocado.\" " & 
    sleep 20
    let i=$i+1
done
echo "NQPs no ar"
sleep 1m
cd $PARGRES_HOME/bin
chmod +x ./run_cqp_duckdb.sh
echo "Executando CPQ..."
./run_cqp_duckdb.sh $WORKDIR/config/config.xml  > $WORKDIR/run_cqp_duckdb.out 2> $WORKDIR/run_cqp_duckdb.err &
echo "Início da pausa para carga de metadados:" `date`
sleep 10m
echo "Final da pausa para carga de metadados:" `date`

cd $DIR_INICIAL
echo "Fim start"
exit 0
