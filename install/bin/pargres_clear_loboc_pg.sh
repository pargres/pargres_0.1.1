#!/bin/bash

# Shell script para remover os dados e diretórios criados especificamente para execucao no LoboC.
# O $WORKDIR nao sera apagado no momento. O objetivo é tornar possivel a analise da execucao a posteriori.
#
# Parâmetros:
#  - $1 - diretorio de trabalho na area de SCRATCH
#  - $2 - arquivo com os nomes dos nós alocados para o job

if [ $# -ne 2 ];
then
    echo "usage: pargres_clear_loboc_pg.sh WORKDIR NODEFILE"
    exit 2
fi

WORKDIR=$1
NODEFILE=$2

# Apagar replicas da base de dados
i=0
for line in $(cat $NODEFILE)
do
    rm -R $WORKDIR/datadir_$i
    let i=$i+1
done

exit 0
