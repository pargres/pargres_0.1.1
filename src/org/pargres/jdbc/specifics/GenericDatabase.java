package org.pargres.jdbc.specifics;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.pargres.commons.Range;
import org.pargres.commons.logger.Logger;
import org.pargres.commons.translation.Messages;

public class GenericDatabase extends AbstractDatabase implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static Logger logger = Logger.getLogger(GenericDatabase.class);

    public ArrayList<Range> loadPartitionedColumns(DatabaseProperties prop, String tableName,
            Connection con, int type) throws SQLException {
        logger.debug(Messages.getString("genericdatabase.loadPartitionedColumns", new Object[]{tableName}));
        ArrayList<Range> rangeList = new ArrayList<Range>();
        tableName = tableName.toLowerCase();

        if (prop.containsKey(tableName.toLowerCase())) {
            String[] values = ((String) prop.get(tableName)).split(",");
            for (int i = 0; i < values.length; i++) {

                String limit = "";
                java.sql.Statement stMax = con.createStatement();
                stMax.setMaxRows(1);
                /* 2020-06-11 - Assis: 
                        Using the query below in order to obtain the maximum value is strange. 
                        max() is part of SQL standard.
                logger.debug(Messages.getString("genericdatabase.loadPartitionedColumns.maxvaluequery", new Object[]{"SELECT " + values[i] + " as pargres_maxValue FROM " + tableName + " ORDER BY " + values[i] + " DESC " + limit}));
                ResultSet rsMax = con.createStatement().executeQuery("SELECT " + values[i]
                        + " as pargres_maxValue FROM " + tableName + " ORDER BY " + values[i] + " DESC "
                        + limit);
                Below, a faster query:
                */
                logger.debug(Messages.getString("genericdatabase.loadPartitionedColumns.maxvaluequery", new Object[]{"SELECT max(" + values[i] + ") as pargres_maxvalue FROM " + tableName}));
                ResultSet rsMax = con.createStatement().executeQuery("SELECT max(" + values[i] 
                        + ") as pargres_maxvalue FROM " + tableName);
                long rangeEnd = 0;
                String rangeMax = null;
                if (type == PARTITIONED_COLUMNS) {
                    if (rsMax.next()) {
                        rangeEnd = rsMax.getLong("pargres_maxvalue");
                    }
                } else if (type == ORDER_BY_COLUMNS) {
                    if (rsMax.next()) {
                        rangeMax = rsMax.getString("pargres_maxvalue");
                    }
                }

                java.sql.Statement stMin = con.createStatement();
                stMin.setMaxRows(1);
                /* 2020-06-11 - Assis: 
                        Using the query below in order to obtain the minimum value is strange. 
                        min() is part of SQL standard.
                logger.debug(Messages.getString("genericdatabase.loadPartitionedColumns.minvaluequery", new Object[]{"SELECT " + values[i] + " as pargres_minValue FROM " + tableName + " ORDER BY " + values[i] + " ASC " + limit}));
                ResultSet rsMin = stMin.executeQuery("SELECT " + values[i] + " as pargres_minValue FROM "
                        + tableName + " ORDER BY " + values[i] + " ASC " + limit);
                Below, a faster query:
                */
                logger.debug(Messages.getString("genericdatabase.loadPartitionedColumns.minvaluequery", new Object[]{"SELECT min(" + values[i] + ") as pargres_minvalue FROM " + tableName}));
                ResultSet rsMin = stMin.executeQuery("SELECT min(" + values[i] + ") as pargres_minvalue FROM "
                        + tableName);
                
                long rangeInit = 0;
                String rangeMin = null;
                if (type == PARTITIONED_COLUMNS) {
                    if (rsMin.next()) {
                        rangeInit = rsMin.getLong("pargres_minvalue");
                    }
                } else if (type == ORDER_BY_COLUMNS) {
                    if (rsMin.next()) {
                        rangeMin = rsMin.getString("pargres_minvalue");
                    }
                }

                java.sql.Statement stCardinality = con.createStatement();
                //stCardinality.setMaxRows(1);
                logger.debug(Messages.getString("genericdatabase.loadPartitionedColumns.countquery", new Object[]{"select count(*) as pargres_reltuples from " + tableName}));
                ResultSet rsCardinality = stCardinality.executeQuery(
                        "select count(*) as pargres_reltuples from " + tableName);
                long cardinality = 0;
                if (rsCardinality.next()) {
                    cardinality = rsCardinality.getLong("pargres_reltuples");
                }

                Range range = new Range(values[i], tableName, rangeInit, rangeEnd, cardinality);
                range.setRangeMax(rangeMax);
                range.setRangeMin(rangeMin);
                rangeList.add(range);
            }
        }
        return rangeList;
    }
}
