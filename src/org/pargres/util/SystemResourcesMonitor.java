/*
 * Monitor.java
 *
 * Created on September 22, 2004, 2:34 PM
 */

package org.pargres.util;

import java.io.BufferedReader;
import java.io.IOException;

/**
 *
 * @author  assis
 */

public class SystemResourcesMonitor implements Runnable{

	private static BufferedReader reader;
    private static Process proc;
    private static String vmstatLine="";
    private static boolean terminationRequested;
    private static double blocksReadCounter;
    private static double blocksWrittenCounter;
    /** Creates a new instance of Monitor */
    public SystemResourcesMonitor() throws IOException {
    	return;
    }

    protected void finalize() throws Throwable {
        if ( proc!=null ) {
            proc.destroy();
            proc=null;
        }
    }

    public void start() {
        Thread t = new Thread( this );
        t.start();
    }

    public void run() {
    	return;
    }

    public synchronized void finish() {
        if (!terminationRequested)
            terminationRequested = true;
        notifyAll();
    }

    public synchronized void resetCounters() {
        blocksReadCounter = 0;
        blocksWrittenCounter = 0;
        notifyAll();
    }

    public synchronized SystemResourceStatistics getStatistics() {
        SystemResourceStatistics stat = new SystemResourceStatistics();
        stat.setNumberOfBlocksRead( blocksReadCounter );
        stat.setNumberOfBlocksWritten( blocksWrittenCounter );
        notifyAll();
        return stat;
    }

    @SuppressWarnings("unused")
	private static String getVmstatOutput() throws IOException {
        while(reader.ready())
            vmstatLine=reader.readLine();
        return vmstatLine;
    }

    @SuppressWarnings("unused")
	private static long getBlocksRead( String vmstatLine ) throws NumberFormatException, IOException {
        String bi = vmstatLine.substring( 45, 51 );
        return Integer.parseInt( bi.trim() );

    }

    @SuppressWarnings("unused")
	private static long getBlocksWritten( String vmstatLine ) throws NumberFormatException, IOException {
        String bi = vmstatLine.substring( 51, 57 );
        return Integer.parseInt( bi.trim() );

    }

}
