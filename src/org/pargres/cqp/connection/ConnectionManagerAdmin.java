package org.pargres.cqp.connection;

import org.pargres.commons.interfaces.ConnectionManager;
import org.pargres.cqp.ClusterQueryProcessor;
import org.pargres.cqp.connection.ConnectionManagerImpl;
import org.pargres.util.MyRMIRegistry;

public class ConnectionManagerAdmin {
	
	public static void main(String[] args) {
		int port;
        if (args.length == 0) {
            System.out
                    .println("usage: java port");
            return;
        }

        try {
        	port = Integer.parseInt(args[0]);
        	shutdown(port);      	
            	
        } catch (Exception e) {
            System.err.println("Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }
	public static void shutdown(int port)
            throws Exception {
        ConnectionManager cm;

        System.out.println("Shuting down...");
        cm = (ConnectionManager) MyRMIRegistry.lookup("localhost", port,"//localhost:"+port+"/"+ConnectionManager.OBJECT_NAME);
        cm.destroy();
        System.out.println("Done!");
    }
}
